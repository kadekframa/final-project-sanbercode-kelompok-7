<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Forum Diskusi Sederhana</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="{{asset('skydash/vendors/feather/feather.css')}}">
        <link rel="stylesheet" href="{{asset('skydash/vendors/ti-icons/css/themify-icons.css')}}">
        <link rel="stylesheet" href="{{asset('skydash/vendors/css/vendor.bundle.base.css')}}">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet" href="{{asset('skydash/vendors/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('skydash/vendors/select2-bootstrap-theme/select2-bootstrap.min.css')}}">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="{{asset('skydash/css/vertical-layout-light/style.css')}}">
        <!-- endinject -->
        <link rel="shortcut icon" href="{{asset('skydash/images/favicon.png')}}">

        <!-- font awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">
        <!-- end font awesome -->

        <!-- custom css -->
        <link rel="stylesheet" href="{{asset('custom-css/forum.css')}}">
        <!-- end custom css -->

    </head>

    <body>
        <div class="container-scroller">
          <!-- Header Bar -->
          @include('layouts.header')

          <!-- partial -->
          <div class="container-fluid page-body-wrapper">
            <!-- partial:../../partials/_settings-panel.html -->
            <div class="theme-setting-wrapper">
              <div id="settings-trigger"><i class="ti-settings"></i></div>
              <div id="theme-settings" class="settings-panel">
                <i class="settings-close ti-close"></i>
                <p class="settings-heading">SIDEBAR SKINS</p>
                <div class="sidebar-bg-options selected" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Light</div>
                <div class="sidebar-bg-options" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark</div>
                <p class="settings-heading mt-2">HEADER SKINS</p>
                <div class="color-tiles mx-0 px-4">
                  <div class="tiles success"></div>
                  <div class="tiles warning"></div>
                  <div class="tiles danger"></div>
                  <div class="tiles info"></div>
                  <div class="tiles dark"></div>
                  <div class="tiles default"></div>
                </div>
              </div>
            </div>
            <!-- partial -->
            <!-- Sidebar -->
            @include('layouts.sidebar')
            
            <!-- partial -->
            <div class="main-panel">        
              <div class="content-wrapper">
                <div class="row">
                  <div class="col-12 grid-margin stretch-card row">
                    <h3 class="card-title ml-3 mb-4">@yield('title')</h3>
                    @yield('content')
                  </div>
                </div>
              </div>
              <!-- content-wrapper ends -->
              <!-- partial:../../partials/_footer.html -->
              <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                  <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2022. Aplikasi Forum Diskusi Sederhana || Created by Kelompok 7 - Sanbercode Bootcamp Batch 37. All rights reserved.</span>
                  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted &amp; made with <i class="ti-heart text-danger ml-1"></i></span>
                </div>
              </footer>
              <!-- partial -->
            </div>
            <!-- main-panel ends -->
          </div>
          <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="{{asset('skydash/vendors/js/vendor.bundle.base.js')}}"></script>
        <!-- endinject -->
        <!-- Plugin js for this page -->
        <script src="{{asset('skydash/vendors/typeahead.js/typeahead.bundle.min.js')}}"></script>
        <script src="{{asset('skydash/vendors/select2/select2.min.js')}}"></script>
        <!-- End plugin js for this page -->
        <!-- inject:js -->
        <script src="{{asset('skydash/js/off-canvas.js')}}"></script>
        <script src="{{asset('skydash/js/hoverable-collapse.js')}}"></script>
        <script src="{{asset('skydash/js/template.js')}}"></script>
        <script src="{{asset('skydash/js/settings.js')}}"></script>
        <script src="{{asset('skydash/js/todolist.js')}}"></script>
        <!-- endinject -->
        <!-- Custom js for this page-->
        <script src="{{asset('skydash/js/file-upload.js')}}"></script>
        <script src="{{asset('skydash/js/typeahead.js')}}"></script>
        <script src="{{asset('skydash/js/select2.js')}}"></script>
        <!-- End custom js for this page-->
      
      
      
      </body>
</html>
