@extends('layouts.master')

@section('title')
    Halaman List Pertanyaan
@endsection

@section('content')
<div class="table-responsive table-striped">
    <a href="/pertanyaan/create" class="btn btn-primary my-3">Buat Pertanyaan</a>
    <table class="table">
      <thead>
        <tr>
          <th>No.</th>
          <th>User</th>
          <th>Pertanyaan</th>
          <th>Gambar</th>
          <th>Feedback</th>
          <th class="d-flex justify-content-center">Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($pertanyaan as $key=>$pty)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{Str::limit('Kadek Frama', 17)}}</td>
                <td>{{ Str::limit($pty->content_pertanyaan, 94) }}</td>
                <td>
                    <img src="{{asset('images/'.$pty->gambar)}}" alt="ketarangan_gambar">
                </td>
                <td class="col-1">4</td>
                <td class="d-flex justify-content-center">
                    <form action="/pertanyaan/{{$pty->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/pertanyaan/{{$pty->id}}" class="btn btn-success btn-xs mx-1"><i class="fa fa-eye"></i></a>
                        <button type="submit" class="btn btn-danger btn-xs mx-1"><i class="fa fa-trash"></i></button>
                    </form>
                </td>
            </tr>
        @empty
            <h3 class="d-flex justify-content-center m-5">Data Kosong</h3>
        @endforelse
      </tbody>
    </table>
  </div>
@endsection