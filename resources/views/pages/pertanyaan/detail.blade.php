@extends('layouts.detail-master')

@section('title')
    Detail Pertanyaan
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="keterangan-card">Question</h5>
        <div class="container-fluid mt-4">
            <div class="row">
                <div class="col-1 p-0 d-flex justify-content-center">
                    <img src="{{asset('images/1662306577.jpg')}}" alt="gambar_keterangan_pertanyaan" class="img-user">
                </div>
        
                <div class="col-10 p-0">
                    <h4>Kadek Frama</h4>
                    <p class="created_at">{{$pertanyaan->created_at}}</p>
                </div>
        
                <div class="col-1">
                    <button class="btn btn-primary btn-sm" disabled>test</button>
                </div>
            </div>
        
            <div class="row px-3">
                <h3 class="col my-4">{{$pertanyaan->content_pertanyaan}}</h3>
            </div>
        
            <div class="row d-flex justify-content-center">
                <img src="{{asset('images/'.$pertanyaan->gambar)}}" alt="keterangan_gambar_pertanyaan" class="img-gambar-pertanyaan">
            </div>
        
            <div class="row mt-4 px-3">
                <form action="/feedback" method="post" class="col">
                    @csrf
                    <input name="pertanyaan_id" type="text" class="id_invisible" value="{{$pertanyaan->id}}">
                    <textarea name="feedback" class="form-control" rows="4" placeholder="Add Answer..."></textarea>
                    @error('feedback')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    <button type="submit" class="btn btn-outline-success btn-fw my-3">Add</button>
                    <a href="/pertanyaan" class="btn btn-outline-secondary btn-fw my-3 ml-2">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>

@forelse ($feedback as $answer)
    <div class="card mt-3">
        <div class="card-body">
            @if ($checkFeedback == true)
                <div class="row">
                    <h5 class="keterangan-card col-10">Answer</h5>
                    <a class="keterangan-card ml-auto mr-1 btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                    <a class="keterangan-card mr-3 btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                </div>
            @endif
            <div class="container-fluid mt-4">
                <div class="row">
                    <div class="col-1 p-0 d-flex justify-content-center">
                        <img src="{{asset('images/1662306577.jpg')}}" alt="gambar_keterangan_pertanyaan" class="answer-img">
                    </div>
            
                    <div class="col-10 p-0">
                        <h5>Kadek Frama</h5>
                        <p class="created_at">{{$answer->created_at}}</p>
                    </div>
                </div>
            
                <div class="row px-3">
                    <p class="col my-4 answer-text">{{$answer->content_feedback}}</p>
                </div>
            </div>
        </div>
    </div>
@empty
    <div class="card mt-3">
        <div class="card-body">
            <h4 class="d-flex justify-content-center">--Belum Ada Feedback Answer--</h4>
        </div>
    </div>
@endforelse



@endsection