@extends('layouts.master')

@section('title')
    Data Kategori
@endsection

@section('content')
<div class="table-responsive">
    <a href="/kategori/create" class="btn btn-primary my-3">Add Kategori</a>
    <table class="table">
      <thead>
        <tr>
          <th class="col-5">Nama Kategori</th>
          <th>Jml Pertanyaan</th>
          <th class="d-flex justify-content-center">Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($data as $item)
            <tr>
            <td>{{$item->nama}}</td>
            <td>2</td>
            <td class="d-flex justify-content-center">
                <form action="/kategori/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="#" class="btn btn-success btn-xs mx-1">Lihat</a>
                    <button type="submit" class="btn btn-danger btn-xs mx-1">Delete</button>
                </form>
            </td>
            </tr>
        @empty
            <h3 class="d-flex justify-content-center m-5">Data Kosong</h3>
        @endforelse
      </tbody>
    </table>
  </div>
@endsection