@extends('layouts.master')

@section('title')
    Create Kategori
@endsection

@section('content')
    <form action="/kategori" method="POST" class="forms-sample">
        @csrf
        <div class="form-group">
            <label for="exampleInputName1">Nama Kategori</label>
            <input type="text" name="nama" class="form-control" id="exampleInputName1" placeholder="Name...">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <a href="/kategori" class="btn btn-light">Cancel</a>
  </form>
@endsection