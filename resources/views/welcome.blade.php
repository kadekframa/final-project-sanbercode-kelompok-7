<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>forum.com</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('/all/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('/all/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('/all/css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{asset('/all/fonts/icomoon/style.css')}}">
        <link rel="stylesheet" href="{{asset('/all/fonts/feather/style.css')}}">
        <link rel="stylesheet" href="{{asset('/all/fonts/flaticon/font/flaticon.css')}}">
        <link rel="stylesheet" href="{{asset('/all/css/jquery.fancybox.min.css')}}">
        <link rel="stylesheet" href="{{asset('/all/css/aos.css')}}">
        <link rel="stylesheet" href="{{asset('/all/css/style.css')}}">
        <link href="{{asset('/all/plugins/assets/owl.carousel.min.css')}}" rel="stylesheet">
    </head>
    <body>
        <div class="site-mobile-menu site-navbar-target">
          <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close">
              <span class="icofont-close js-menu-toggle"></span>
            </div>
          </div>
          <div class="site-mobile-menu-body"></div>
        </div>
        
        <div class="container">
    
    
          <nav class="site-nav">
            <div class="logo">
              <a href="index.html"><img src="{{asset('/images/logo.png')}}" width="100"></a>
            </div>
            <div class="row align-items-center">
              
              <div class="col-12 col-sm-12 col-lg-12 site-navigation text-center">
                <ul class="js-clone-nav d-none d-lg-inline-block text-left site-menu">
                  <li class="active">
                    @if (Route::has('register'))
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                    @endif
                  </li>
                  
                  <li class="has-children">
                    <a href="#">Masuk</a>
                    <ul class="dropdown">
                      <li><a href="portfolio.html">Admin</a></li>
                      @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('User') }}</a>
                        </li>
                      @endif
                    </ul>
                  </li>
                  <li><a href="#ourServices">Our Services</a></li>
                  <li><a href="#testimonials">Testimonials</a></li>
                </ul>
                <a href="#" class="burger light ml-auto site-menu-toggle js-menu-toggle d-block d-lg-none" data-toggle="collapse" data-target="#main-navbar">
                  <span></span>
                </a>
    
              </div>
    
            </div>  
          </nav> <!-- END nav -->
    
        </div> <!-- END container -->
    
    
        <div class="hero-slant overlay" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('all/images/hero-min.jpg')}}')">
    
          <div class="container">
            <div class="row align-items-center justify-content-between">
              <div class="col-lg-7 intro">
                <h1 class="text-white font-weight-bold mb-4" data-aos="fade-up" data-aos-delay="0">Memudahkan kita dengan bertanya</h1>
                <p class="text-white mb-4" data-aos="fade-up" data-aos-delay="100">Aktif bertanya di kelas merupakan salah satu cara untuk melatih kepercayaan diri kamu. Mengutarakan pendapat dan menanyakan tentang apa yang ingin kamu tanyakan di kelas merupakan salah satu cara untuk melatih kemampuan public speaking.</p>
                <form action="#" class="sign-up-form d-flex" data-aos="fade-up" data-aos-delay="200">
                  <input type="text" class="form-control" placeholder="Masukan pertanyaan anda ...">
                  <input type="submit" class="btn btn-primary" value="Tanya">
                </form>
              
              </div>
              
              
            </div>
    
            
          </div>
    
          <div class="slant" style="background-image: url('{{asset('all/images/slant.svg')}}');"></div>
        </div>
    
        <div class="py-3">
          <div class="container">
    
            <div class="owl-logos owl-carousel">
              <div class="item">
                <img src="{{asset('all/images/logo-puma.png')}}" alt="Image" class="img-fluid">
              </div>
              <div class="item">
                <img src="{{asset('all/images/logo-adobe.png')}}" alt="Image" class="img-fluid">
              </div>
              <div class="item">
                <img src="{{asset('all/images/logo-google.png')}}" alt="Image" class="img-fluid">
              </div>
              <div class="item">
                <img src="{{asset('all/images/logo-paypal.png')}}" alt="Image" class="img-fluid">
              </div>
              <div class="item">
                <img src="{{asset('all/images/logo-adobe.png')}}" alt="Image" class="img-fluid">
              </div>
              <div class="item">
                <img src="{{asset('all/images/logo-google.png')}}" alt="Image" class="img-fluid">
              </div>
              
    
            </div>
            
                
          </div>
          
        </div>
        
         <div class="site-section">
          <div class="container">
            <div class="row mb-5">
              <div class="col-12 text-center"  data-aos="fade-up">
                <h2 class="heading font-weight-bold mb-3" id="ourServices">Our Services</h2>
              </div>
            </div>
            <div class="row align-items-stretch">
              <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4">
                    <span class="feather-pen-tool"></span>
                  </div>
                  <div>
                    <h3>Modern Design</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="100">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4">
                    <span class="feather-layers"></span>
                  </div>
                  <div>
                    <h3>High Performance</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="200">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4">
                    <span class="feather-layout"></span>
                  </div>
                  <div>
                    <h3>Flexible Layout</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
    
    
              <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="300">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4">
                    <span class="feather-life-buoy"></span>
                  </div>
                  <div>
                    <h3>Free Support</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="400">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4">
                    <span class="feather-shopping-bag"></span>
                  </div>
                  <div>
                    <h3>Cool Pricing</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up" data-aos-delay="500">
                <div class="unit-4 d-flex">
                  <div class="unit-4-icon mr-4">
                    <span class="feather-smartphone"></span>
                  </div>
                  <div>
                    <h3>Mobile Apps</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#">Learn More</a></p>
                  </div>
                </div>
              </div>
    
            </div>
          </div>
        </div>
        <br><br><br><br>
         <!-- ======= Testimonials Section ======= -->
        <section id="testimonials" class="testimonials">
        <div class="container">
  
          <div class="section-title" data-aos="fade-up">
            <h2 id="testimonials">Testimonials</h2>
            <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
          </div>
  
          <div class="owl-carousel testimonials-carousel" data-aos="fade-up" data-aos-delay="100">
  
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('all/plugins2/img/testimonials/testimonials-1.jpg')}}" class="testimonial-img" alt="">
              <h3>Saul Goodman</h3>
              <h4>Ceo &amp; Founder</h4>
            </div>
  
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('all/plugins2/img/testimonials/testimonials-2.jpg')}}" class="testimonial-img" alt="">
              <h3>Sara Wilsson</h3>
              <h4>Designer</h4>
            </div>
  
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('all/plugins2/img/testimonials/testimonials-3.jpg')}}" class="testimonial-img" alt="">
              <h3>Jena Karlis</h3>
              <h4>Store Owner</h4>
            </div>
  
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('all/plugins2/img/testimonials/testimonials-4.jpg')}}" class="testimonial-img" alt="">
              <h3>Matt Brandon</h3>
              <h4>Freelancer</h4>
            </div>
  
            <div class="testimonial-item">
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
              <img src="{{asset('all/plugins2/img/testimonials/testimonials-5.jpg')}}" class="testimonial-img" alt="">
              <h3>John Larson</h3>
              <h4>Entrepreneur</h4>
            </div>
  
          </div>
  
        </div>
        </section><!-- End Testimonials Section -->
    
        <br><br><br><br>
         <div class="site-footer">
          <div class="container">
            <div class="row justify-content-between">
              <div class="col-lg-4">
                <div class="widget">
                  <h3>About</h3>
                  <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live.</p>
                </div>
                <div class="widget">
                  <h3>Connect with us</h3>
                  <ul class="social list-unstyled">
                    <li><a href="#"><span class="icon-facebook"></span></a></li>
                    <li><a href="#"><span class="icon-twitter"></span></a></li>
                    <li><a href="#"><span class="icon-instagram"></span></a></li>
                    <li><a href="#"><span class="icon-dribbble"></span></a></li>
                    <li><a href="#"><span class="icon-linkedin"></span></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="row">
                  <div class="col-12">
                    <div class="widget">
                      <h3>Navigations</h3>
                    </div>
                  </div>
                  <div class="col-6 col-sm-6 col-md-4">
                    <div class="widget">
                      <ul class="links list-unstyled">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Work</a></li>
                        <li><a href="#">Process</a></li>
                        <li><a href="#">About Us</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-6 col-sm-6 col-md-4">
                    <div class="widget">
                      <ul class="links list-unstyled">
                        <li><a href="#">Press</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Privacy</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-6 col-sm-6 col-md-4">
                    <div class="widget">
                      <ul class="links list-unstyled">
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Process</a></li>
                        <li><a href="#">About Us</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="row justify-content-center text-center copyright">
              <div class="col-md-8">
                <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Aplikasi Forum Diskusi Sederhana || Created by Kelompok 7 - Sanbercode Bootcamp Batch 37. All rights reserved
                </p>
              </div>
            </div>
          </div>
        </div>
        <div id="overlayer"></div>
        <div class="loader">
          <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>

        <script src="{{asset('/all/js/jquery-3.5.1.min.js')}}"></script>
        <script src="{{asset('/all/js/jquery-migrate-3.0.0.min.js')}}"></script>
        <script src="{{asset('/all/js/popper.min.js')}}"></script>
        <script src="{{asset('/all/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('/all/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('/all/js/aos.js')}}"></script>
        <script src="{{asset('/all/js/imagesloaded.pkgd.js')}}"></script>
        <script src="{{asset('/all/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('/all/js/jquery.animateNumber.min.js')}}"></script>
        <script src="{{asset('/all/js/jquery.stellar.min.js')}}"></script>
        <script src="{{asset('/all/js/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('/all/js/jquery.fancybox.min.js')}}"></script>
        <script src="{{asset('/all/js/custom.js')}}"></script>
        <script src="{{asset('/all/plugins2/js/owl.carousel.min')}}"></script>
        

        <script src="{{asset('/all/plugins2/js/main.js')}}"></script>
    </body>
</html>
