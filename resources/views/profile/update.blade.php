@extends('layouts.master')

@section('title')
    Updte Profile
@endsection

@section('content')
    <form action="/profile/{{$profile->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="biodata" class="form-control" id="" cols="30" rows="10">{{old('bio', $profile->biodata)}}</textarea>
        </div>
        @error('biodata')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <div class="form-group">
            <label>Email</label>
           <textarea name="email" class="form-control">{{old('email', $profile->email)}}</textarea>
        </div>
        @error('email')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" value="{{old('umur', $profile->umur)}}" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat"class="form-control" id="" cols="30" rows="10">{{old('alamat', $profile->alamat)}}</textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection