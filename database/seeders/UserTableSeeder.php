<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'name'      =>  'Admin',
            'email'     =>  'admin@example.com',
            'password'  =>  bcrypt('password'),
            'type'      =>  User::ADMIN,
        ]);

        User::factory()->count(5)->create();
    }
}
