<?php

namespace App\Http\Middleware;
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Closure;
use Illuminate\Http\Request;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::guard($guard)->user()->can(UserPolicy::ADMIN, User::class)){
            return $next($request);
        }
            
        throw new HttpException(403, 'Forbidden');
    }

//     class User
// {
//     use HasFactory;
//     ...
// }

}
