<?php


namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LoginResponse as ContractsLogInResponse;

class LoginResponse implements ContractsLogInResponse
{
    public function toResponse($request)
    {
        if(auth()->user()->isAdmin()){
            return redirect()->route('admin.index');
        }
        return redirect()->intended(config('fortify.home'));
    }
}