<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function index() {
        $data = Kategori::get();

        return view('pages.kategori.index', ['data' => $data]);
    }

    public function create() {
        return view('pages.kategori.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
        ],
        [
            'nama.required' => 'Beri nama kategori baru yang akan ditambahkan!',
        ]);

        $kategori = new Kategori;
        $kategori->nama = $request['nama'];
        $kategori->save();

        return redirect('/kategori');
    }

    public function delete($id) {
        Kategori::where('id', $id)->delete();
        
        return redirect('/kategori');
    }
}