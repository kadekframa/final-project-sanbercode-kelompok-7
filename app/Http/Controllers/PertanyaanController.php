<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use App\Models\Pertanyaan;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::get();
        
        return view('pages.pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $kategori = Kategori::get();

        return view('pages.pertanyaan.create', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'pertanyaan' => 'required',
            'gambar' => 'required|image|mimes:jpg,png,jpeg',
            'kategori_id' => 'required',
        ],
        [
            'pertanyaan.required' => 'Tambahkan pertanyaan yang ingin ditanyakan!',
            'gambar.required' => 'Cantumkan gambar untuk membantu menjelaskan pertanyaan Anda!',
            'kategori_id.required' => 'Pilih kategori dari pertanyaan yang dibuat!',
        ]);
        
        $filename = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('images'), $filename);

        $pertanyaan = new Pertanyaan;
        $pertanyaan->content_pertanyaan = $request->pertanyaan;
        $pertanyaan->gambar = $filename;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->user_id = Auth::id();

        $pertanyaan->save();

        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detailPertanyaan = Pertanyaan::where('id', $id)->first();
        $feedbackAnswer = Feedback::where('pertanyaan_id', $id)->get();
        $checkFeedback = Feedback::where('user_id', Auth::id())->get();

        return view('pages.pertanyaan.detail', ['pertanyaan' => $detailPertanyaan, 'feedback' => $feedbackAnswer, 'checkFeedback' => $checkFeedback]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pertanyaan::where('id', $id)->delete();

        return redirect('/pertanyaan');
    }
}
