<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function index()
    {
        $iduser = Auth::id();
        
        $profile = Profile::where('user_id', $iduser)->first();
        
        return view('profile.update',['profile'=>$profile]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'biodata' => 'required',
            'email' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
        ]);

        $profile = Profile::find($id);

        $profile->biodata = $request->biodata;
        $profile->email = $request->email;
        $profile->umur = $request->umur;
        $profile->alamat= $request->alamat;

        $profile->save();

        return redirect('/profile');
    }
}
