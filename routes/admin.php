<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'as' => 'admin'], function (){
    Route::get('/', [DashboardController::class])->name('index');
});

class User
{
    use HasFactory;
   
}