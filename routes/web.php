<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\dashboardController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\FeedbackController;

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/kategori/create', function () {
//     return view('pages.kategori.create');
// });

Route::get('/', [IndexController::class, 'home']);
Route::get('/dashboard', [dashboardController::class, 'dashboard']);

//Create Data
//Route untuk mengarah ke form tambah kategori
Route::get('/kategori', [KategoriController::class, 'index']);
Route::get('/kategori/create', [KategoriController::class, 'create']);
Route::post('/kategori', [KategoriController::class, 'store']);
Route::delete('/kategori/{kategori_id}', [KategoriController::class, 'delete']);


// CRUD Pertanyaan
Route::resource('pertanyaan', PertanyaanController::class);

// CRUD Feedback Response
Route::resource('feedback', FeedbackController::class);

// CRUD Profile
Route::resource('profile', ProfileController::class)->only('index','update');
Auth::routes();
